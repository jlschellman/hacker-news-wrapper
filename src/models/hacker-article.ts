import { Prop, raw, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';


export class HackerArticle {
  created_at: string;
  title: string;
  url: string;
  author: string;
  points: number;
  story_text: string;
  comment_text: string;
  num_comments: number;
  story_id: number;
  story_title: string;
  story_url: string;
  parent_id: number;
  created_at_i: number;
  _tags: Array<string>;
  objectID: string;
  _highlightResult: HighlightResult;

  constructor(partial: Partial<HackerArticle>) {
    Object.assign(this, partial);
  }
}


interface HighlightResult {
  title?: HighlightResultItem;
  url?: HighlightResultItem;
  author?: HighlightResultItem;
  comment_text?: HighlightResultItem;
  story_title?: HighlightResultItem;
  story_url?: HighlightResultItem;
}

interface HighlightResultItem {
  value: string;
  matchLevel: string;
  matchedWords: Array<string>;
  fullyHighlighted?: boolean;
}



export class HackerNewsResult {
  hits: Array<HackerArticle>;
  nbHits:	number;  
  page: number;
  nbPages: number;
  hitsPerPage: number;
  exhaustiveNbHits: boolean;
  exhaustiveTypo: boolean;
  query: string;
  params: string;
  renderingContent: {};
  processingTimeMS: number;

  constructor(partial: Partial<HackerNewsResult>) {
    Object.assign(this, partial);
  }

}


export type HackerNewsArticleDocument = HackerArticle & Document;

const highlightResultItemSubSchema = new mongoose.Schema({
  value: String,
  matchLevel: String,
  matchedWords: [String],
  fullyHighlighted: Boolean
})

const highlightResultSubSchema = new mongoose.Schema({
  title: highlightResultItemSubSchema,
  url: highlightResultItemSubSchema,
  author: highlightResultItemSubSchema,
  comment_text: highlightResultItemSubSchema,
  story_title: highlightResultItemSubSchema,
  story_url: highlightResultItemSubSchema,
})

export const HackerArticleSchema = new mongoose.Schema({
  created_at: String,
  title: String,
  url: String,
  author: String,
  points: Number,
  story_text: String,
  comment_text: String,
  num_comments: Number,
  story_id: Number,
  story_title: String,
  story_url: String,
  parent_id: Number,
  created_at_i: Number,
  _tags: [String],
  objectID: String,
  _highlightResult: highlightResultSubSchema,
  isDisabled: {
    type: Boolean,
    default: false
  }
})


