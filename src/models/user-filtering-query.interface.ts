export interface UserFilteringQuery {
  author?: string;
  tags?: string;
  _tags?: { $all:  Array<string> };
  title?: string;
  page?: number;
}
