import { Controller, Delete, Get, Query } from '@nestjs/common';
import { AppService } from './app.service';
import { UserFilteringQuery } from './models/user-filtering-query.interface';
import { HackerArticlesQueryPipe } from './pipes/hacker-articles-query.pipe';

@Controller("")
export class AppController {
  constructor(private readonly appService: AppService) {}

  /**
   * This endpoint may be removed if a manual insert 
   * shouldn't be provided to users
   */
  @Get("manual_insert")
  hourlyInsertIntoDb() {
    return this.appService.hourlyInsertIntoDb();
  }

  /**
   * Retrieves Hacker News articles from DB that are not removed
   * @param filterQuery User query based on author, tags and article title
   * @returns 
   */
  @Get("articles")
  getArticles(@Query(new HackerArticlesQueryPipe()) filterQuery: UserFilteringQuery) {
    return this.appService.getArticles(filterQuery);
  }

  /**
   * Removes articles from ever showing up as a result to this API,
   * but it does not remove them from the DB
   * @param filterQuery User query based on author, tags and article title
   * @returns 
   */
  @Delete("articles")
  deleteArticles(@Query(new HackerArticlesQueryPipe()) filterQuery: UserFilteringQuery) {
    return this.appService.removeItems(filterQuery);
  }

}
