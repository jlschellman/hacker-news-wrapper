import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { InjectModel } from '@nestjs/mongoose';
import { Cron } from '@nestjs/schedule';

import { lastValueFrom } from 'rxjs';
import { Model } from 'mongoose';

import { UserFilteringQuery } from './models/user-filtering-query.interface';
import { HackerArticle, HackerNewsArticleDocument, HackerNewsResult } from './models/hacker-article';

@Injectable()
export class AppService {

  constructor(
    private httpService: HttpService,
    @InjectModel(HackerArticle.name) private articleModel: Model<HackerNewsArticleDocument>,
  ) {}


  @Cron('0 * * * *')
  async hourlyInsertIntoDb(): Promise<HackerArticle[]> {
    const res = await lastValueFrom(
      this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs').pipe()
    );
    
    console.log("retrieved articles:", res.data);
    
    const articlesResult = new HackerNewsResult(res.data);
    const articlesHits = articlesResult.hits.map(article => new HackerArticle(article))

    // Saves on DB only new entries, otherwise update
    // Criteria for repetition based on HackerArticle.objectID
    for (const article of articlesHits) {
      const articleModel = new this.articleModel(article)
      await this.articleModel.findOneAndUpdate({ objectID:  article.objectID }, articleModel, {
        new: true, // return the updated/inserted doc
        upsert: true
      });
    }

    return articlesHits;
  }


  getArticles(query: UserFilteringQuery) {
    // 1-based index for pagination
    const paginationOptions = {
      page: "page" in query ? query.page : 1,
      limit: 5 
    }

    // Retrieve only non-disabled documents
    Object.assign(query, { isDisabled: false });

    return this.articleModel
      .find(query)
      .skip((paginationOptions.page-1) * paginationOptions.limit)
      .limit(paginationOptions.limit);

  }

  async removeItems(query: UserFilteringQuery): Promise<string> {
    const res = await this.articleModel.updateMany(query, { isDisabled: true })
    return `${res.modifiedCount} article(s) deleted with params ${query}`;
  }

}
