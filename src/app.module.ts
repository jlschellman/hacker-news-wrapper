import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HackerArticle, HackerArticleSchema } from './models/hacker-article';
import { ScheduleModule } from '@nestjs/schedule';

/**
 * @TODO mognodb user and password goes in env files
 */
@Module({
  imports: [
    HttpModule,
    ScheduleModule.forRoot(),
    MongooseModule.forRoot('mongodb://reign:reign@localhost:27017/articles?authSource=admin&readPreference=primary'),
    MongooseModule.forFeature([{
      name: HackerArticle.name, 
      schema:  HackerArticleSchema
    }])
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

