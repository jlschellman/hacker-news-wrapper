import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { UserFilteringQuery } from '../models/user-filtering-query.interface';

@Injectable()
export class HackerArticlesQueryPipe implements PipeTransform {
  
  // Rename field "tags" to correctly database named "_tags"
  transform(query: UserFilteringQuery, metadata: ArgumentMetadata) {
    if ("tags" in query) {
      const splittedTags = query.tags.split(",");
      Object.defineProperty(query, "_tags",
          Object.getOwnPropertyDescriptor(query, "tags"));
      delete query["tags"];

      query._tags = { $all: splittedTags };
    }
    
    return query;
  }
}
