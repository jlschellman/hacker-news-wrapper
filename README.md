## Description

REST API based on [NestJS](https://github.com/nestjs/nest) that wraps [Hacker News](https://news.ycombinator.com/) from YCombinator. This API allows for retrieving posts and can be filtered by author, tags and/or post's title. Articles are retrieved and saved hourly to a Mongo database (and can be done manually, tentatively). Article queries to this API retrieves those same articles to a Mongo database.

## Setting UP

Installation of node based packages
```bash
$ npm install
```

Local deployment of MongoDB container service
```bash
$ docker-compose up -d
```


From the docker-compose.yml file and the app.module.ts file one can see, firstly, that the username and password for the DB are the same, ``reign``, and that the full URI for connection is ``mongodb://reign:reign@localhost:27017/articles?authSource=admin&readPreference=primary``.
This poses a problem if this app is to be deployed on production, since sensible data such as database credentials are to be located in an env file, so this app should only, for now, run locally on development. 

## Running the app

```bash
$ npm run start
```

Description for endpoints and examples can be found at the publicly available [Postman documentation for this project ](https://documenter.getpostman.com/view/18927110/UVRHh3KM#2a1afb65-8dfe-48f1-b7f4-3d6f814e3261) 


## Credits

- Author - [Jorge Schellman](https://www.linkedin.com/in/jorge-schellman-103535117/)


